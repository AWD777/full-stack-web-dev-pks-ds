<?php

namespace App\Mail;

use App\User;
use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegenerateUserMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $otp_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,OtpCode $otp_code)
    {
        $this->user=$user;
        $this->otp_code=$otp_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.user.user_regenerate_email');
    }
}
