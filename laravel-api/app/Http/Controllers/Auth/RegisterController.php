<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Events\RegisterUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'   => 'required|unique:users,username',
            'name' => 'required',
            'email' => 'required|unique:users,email|email'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($request->all());

        do {
            $randomOtp = mt_rand(100000, 999999);
            $checkOtp = OtpCode::where('otp', $randomOtp)->first();
        } while ($checkOtp);

        $optUntil = Carbon::now()->addMinutes(5);

        $otp_code = OtpCode::create([
            'otp' => $randomOtp,
            'valid_until' => $optUntil,
            'user_id' => $user->id
        ]);
        
        event(new RegisterUser($user,$otp_code));

        return response()->json([
            'success' => true,
            'message' => 'User telah dibuat. Silahkan verifikasi email',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
