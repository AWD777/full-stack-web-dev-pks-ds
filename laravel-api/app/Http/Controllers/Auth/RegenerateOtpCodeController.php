<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Illuminate\Http\Request;
use App\Events\RegenerateUser;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();
        if ($user->otpCode) {
            $user->otpCode->delete();
        }

        do {
            $randomOtp = mt_rand(100000, 999999);
            $checkOtp = OtpCode::where('otp', $randomOtp)->first();
        } while ($checkOtp);

        $optUntil = Carbon::now()->addMinutes(5);

        $otp_code = OtpCode::create([
            'otp' => $randomOtp,
            'valid_until' => $optUntil,
            'user_id' => $user->id
        ]);

        event(new RegenerateUser($user,$otp_code));

        return response()->json([
            'success' => true,
            'message' => 'OTP telah diregenerasi. Silahkan cek email anda',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
