<?php

namespace App\Events;

use App\OtpCode;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
Use App\User;

class RegisterUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $otp_code;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user,OtpCode $otp_code)
    {
        $this->user=$user;
        $this->otp_code=$otp_code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
