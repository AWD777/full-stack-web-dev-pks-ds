<?php

namespace App\Events;

use App\User;
use App\OtpCode;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RegenerateUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $otp_code;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user,OtpCode $otp_code)
    {
        $this->user=$user;
        $this->otp_code=$otp_code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
