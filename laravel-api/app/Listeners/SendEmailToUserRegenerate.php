<?php

namespace App\Listeners;

use App\Events\RegenerateUser;
use App\Mail\RegenerateUserMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToUserRegenerate implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateUser  $event
     * @return void
     */
    public function handle(RegenerateUser $event)
    {
        Mail::to($event->user->email)->send(new RegenerateUserMail($event->user,$event->otp_code));
    }
}
