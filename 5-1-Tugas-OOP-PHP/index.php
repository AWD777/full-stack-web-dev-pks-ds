<?php
// Parent class
trait Hewan{
  public $nama;
  public $darah=50;
  public $jumlahKaki;
  public $keahlian;

  public function atraksi(){
    //di dalam method ini akan menampilkan string nama dan keahlian. Contoh "harimau_1 sedang lari cepat" atau "elang_3 sedang terbang tinggi".
    return '<h2>Atraksi Hewan  '.$this->nama.'</h2>'.$this->nama.' sedang '.$this->keahlian.'<br>';
  }
}

trait Fight{
  public $attackPower;
  public $defencePower;

  public function serang($hewan2){
    //di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang menyerang elang_3" atau "elang_3 sedang menyerang harimau_2".
    echo '<h2>Fight</h2>Hewan '.$this->nama.' sedang menyerang '.$hewan2->nama.'<br>';
    self::diserang($this,$hewan2);
  }

  public function diserang($hewan1,$hewan2){
    //di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang diserang" atau "elang_3 sedang diserang", kemudian hewan yang diserang akan berkurang darahnya dengan rumus :
    // "darah sekarang - attackPower penyerang / defencePower yang diserang"
    // method diserang() otomatis dipanggil jika method serang() dipanggil.
    $darahsekarang=$hewan2->darah;
    $hewan2->darah=$darahsekarang-$hewan1->attackPower/$hewan2->defencePower;
    echo 'Hewan '.$hewan2->nama.' sedang diserang '.$hewan1->nama.'<br>Darah '.$darahsekarang.' berkurang menjadi '.$hewan2->darah.'<br>';
  }
}

trait infoHewan{
  public function getInfoHewan(){
    // didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau).
    return  '<h2>Informasi Hewan '.$this->nama.'</h2>
           Nama Hewan : '.$this->nama.'<br>
           Banyak Darah : '.$this->darah.'<br>
           Banyak Kaki : '.$this->jumlahKaki.'<br>
           Keahlian : '.$this->keahlian.'<br>
           Attack : '.$this->attackPower.'<br>
           Defence : '.$this->defencePower.'<br>
           Jenis hewan : '. get_class().'<br>';
  }
}

class Elang{
  use Hewan,Fight,infoHewan;
  // Ketika Elang diinstansiasi, maka jumlahKaki bernilai 2, dan keahlian bernilai "terbang tinggi", attackPower = 10 , deffencePower = 5 ;
  public function __construct(){
    $this->nama='Elang';
    $this->jumlahKaki=2;
    $this->keahlian='terbang tinggi';
    $this->attackPower=10;
    $this->defencePower=5;
  }
}

class Harimau{
  use Hewan,Fight,infoHewan;
  // Ketika Harimau diintansiasi, maka jumlahKaki bernilai 4, dan keahlian bernilai "lari cepat" , attackPower = 7 , deffencePower = 8 ;
  public function __construct(){
    $this->nama='Harimau';
    $this->jumlahKaki=4;
    $this->keahlian='lari cepat';
    $this->attackPower=7;
    $this->defencePower=8;
  }
}

$elang=new Elang();
$harimau=new Harimau();

echo $elang->getInfoHewan();
echo '==================================================<br>';

echo  $harimau->getInfoHewan();
echo '==================================================<br>';

echo $elang->atraksi();
echo '==================================================<br>';

echo $harimau->atraksi();
echo '==================================================<br>';

$elang->serang($harimau);
echo '==================================================<br>';

$harimau->serang($elang);
echo '==================================================<br>';

echo $elang->getInfoHewan().$harimau->getInfoHewan();