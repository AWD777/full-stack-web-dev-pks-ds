//==============================================================================================================
//Soal No. 1
//buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

//jawaban
console.log('Soal No. 1')
const sisi=2
const persegiPanjang = (p=0,l=0) => {
    return{
        luas:p*l,
        keliling:sisi*(p+l)
    }
}

let panjang=3
let lebar=5

console.log('Luas persegi panjang = ',persegiPanjang(panjang,lebar).luas)
console.log('Keliling persegi panjang = ',persegiPanjang(panjang,lebar).keliling)
console.log()
//==============================================================================================================

//==============================================================================================================
//Soal No. 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

//jawaban
console.log('Soal No. 2')
const newFunction = (firstName, lastName)=>{
  return {
    firstName,
    lastName,
    fullName: ()=>{
      console.log(`${firstName} ${lastName}`)
    }
  }
}
newFunction("William", "Imoh").fullName()
console.log()
//==============================================================================================================

//==============================================================================================================
//Soal No. 3
// Diberikan sebuah objek sebagai berikut:
console.log('Soal No. 3')
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

//jawaban
const {firstName, lastName,address,hobby} = newObject
// Driver code
console.log(firstName, lastName, address, hobby)
console.log()
//==============================================================================================================

//==============================================================================================================
//Soal No. 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
console.log('Soal No. 4')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

//jawaban
let combined=[...west,...east]
//Driver Code
console.log(combined)
console.log()
//==============================================================================================================

//==============================================================================================================
//Soal No. 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
console.log('Soal No. 5')
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
// console.log(before)

//jawaban
const after=`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)
//==============================================================================================================