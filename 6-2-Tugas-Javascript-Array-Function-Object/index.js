//===============================================================================================
//Soal No. 1
// buatlah variabel seperti di bawah ini
// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
//Jawaban Soal No. 1
console.log("Soal No. 1")
daftarHewan.sort()
daftarHewan.forEach(function(item){
    console.log(item)
 })
//===============================================================================================

//===============================================================================================
//Soal No. 2
//Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
//Jawaban Soal No. 2
console.log()
console.log("Soal No. 2")

function introduce(params) {
    return "Nama saya "+params.name+", umur saya "+params.age+" tahun, alamat saya di "+params.address+", dan saya punya hobby yaitu "+params.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) 
//===============================================================================================

//===============================================================================================
//Soal No. 3
//Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
//Jawaban No. 3
console.log()
console.log("Soal No. 3")
function hitung_huruf_vokal(params) {
    var karakter = params.match(/[aeiou]/gi)
    return karakter.length
  }

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2
//===============================================================================================

//===============================================================================================
//Soal No. 4
//Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.
//Jawaban No. 4
console.log()
console.log("Soal No. 4")
function hitung(params) {
    var operasi=params*2-2;
    return operasi;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
//===============================================================================================